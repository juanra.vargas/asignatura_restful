package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignaturas;
import py.una.pol.personas.model.Persona;


@Stateless
public class AsignaturaDAO {
 
	
    @Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */
	public List<Asignaturas> seleccionar() {
		String query = "SELECT idasignatura, nombreasignatura FROM asignaturas ";
		
		List<Asignaturas> lista = new ArrayList<Asignaturas>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Asignaturas a = new Asignaturas();
        		a.setID(rs.getLong(1));
        		a.setNombre(rs.getString(2));
        		
        		lista.add(a);        		    		
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public Asignaturas seleccionarPorID(long ID) {
		String SQL = "SELECT idasignatura, nombreasignatura FROM asignaturas WHERE idasignatura = ? ";
		Asignaturas a = null;
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, ID);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		a = new Asignaturas();
        		a.setID(rs.getLong(1));
        		a.setNombre(rs.getString(2));
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return a;

	}
	
	
    public long insertar(Asignaturas a) throws SQLException {

        String SQL = "INSERT INTO asignaturas(idasignatura, nombreasignatura) "
                + "VALUES(?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, a.getId());
            pstmt.setString(2, a.getNombre());
            ///pstmt.setString(3, p.getApellido());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    	
    	
    }
	

    public long actualizar(Asignaturas a) throws SQLException {

        String SQL = "UPDATE asignaturas SET nombreasignatura = ? WHERE idasignatura = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, a.getNombre());
            //pstmt.setString(2, p.getApellido());
            pstmt.setLong(2, a.getId());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
    public long borrar(long ID) throws SQLException {

        String SQL = "DELETE FROM asignaturas WHERE idasignatura = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, ID);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
    
    
    public long seleccionarCodigoCedula(long codigo, long cedula) {

    	String query = "SELECT * FROM relacion where idasignatura = ? and cedula = ?";
		long id = 0;
		int cont = 0;
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs1 = conn.createStatement().executeQuery(query);
        	while (rs1.next()) {
                id = rs1.getLong(1);
                cont ++;
            }
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
    	if (cont == 0) {
    		return 0;
    	}else {
    		return cont;
    	}
        
        
    }
    
    public void AsignarAsignaturas (Long cedula, long a) throws SQLException {
    	//se obtiene la cantidad de filas en Relacion
    	
    	
    	String query = "SELECT * FROM relacion "; 
    	long id = 0;
		//List<Asignaturas> lista = new ArrayList<Asignaturas>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs1 = conn.createStatement().executeQuery(query);
        	while (rs1.next()) {
                id = rs1.getLong(1);
            }
        	id = id + 1;
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
    	
        
        
        
        
    	
    	/**
    	 * Se realiza el insert en la tabla relacion
    	 * */
        
    	String SQL = "INSERT INTO relacion(idrelacion, idasignatura, cedula) "
                 + "VALUES(?,?,?)";
    	 Connection conn2 = null;
         
         try 
         {
         	conn2 = Bd.connect();
         	PreparedStatement pstmt = conn2.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
         	pstmt.setLong(1, id);
         	pstmt.setLong(2, a);
            pstmt.setLong(3, cedula);
             ///pstmt.setString(3, p.getApellido());
  
             int affectedRows = pstmt.executeUpdate();
             // check the affected rows 
             if (affectedRows > 0) {
                 // get the ID back
                 try (ResultSet rs = pstmt.getGeneratedKeys()) {
                     if (rs.next()) {
                     }
                 } catch (SQLException ex) {
                 	throw ex;
                 }
             }
         } catch (SQLException ex) {
         	throw ex;
         }
         finally  {
         	try{
         		conn2.close();
         	}catch(Exception ef){
         		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
         	}
         }
         //return id2;
    	 
    }
    
    
    public void DesrelacionarAsignatura(long cedula, long a) throws SQLException {
    	String SQL = " DELETE FROM relacion WHERE cedula = ? AND idasignatura = ? ";
    	
    	//long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, a);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        //id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        
        //return id;
    }
    
    public List<Asignaturas> ListarAsignaturasByPersona(long cedula) {
    	
    	String SQL = "select idasignatura, nombreasignatura FROM asignaturas where idasignatura in (select idasignatura from relacion where cedula = ? )";
    	List<Asignaturas> lista = new ArrayList<Asignaturas>();
		
		
		Asignaturas a = null;
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, cedula);
        	
        	ResultSet rs = pstmt.executeQuery();

        	if(rs.next()) {
        		a = new Asignaturas();
        		a.setID(rs.getLong(1));
        		a.setNombre(rs.getString(2));
        		
        		lista.add(a);
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
    }
    
    
    public List<Persona> listarPersonasByAsignatura (long codigo){
    	String query = "SELECT cedula, nombre, apellido FROM persona where cedula in (select cedula from relacion where idasignatura = ? );";
		
		ArrayList<Persona> lista = new ArrayList<Persona>();
		
		Connection conn = null; 
        try 
        {

        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, codigo);
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Persona p = new Persona();
        		p.setCedula(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

    }
    
    
}