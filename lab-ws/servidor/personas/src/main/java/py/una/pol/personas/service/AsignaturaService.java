/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignaturas;
import py.una.pol.personas.model.Persona;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {

    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignaturas a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre());
    }
    public long actualizar(Asignaturas a) throws SQLException {
    	return dao.actualizar(a);
    }
    
     public long actualizar(Asignaturas a) throws SQLException {
    	return dao.actualizar(a);
    }
    public List<Asignaturas> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignaturas seleccionarPorID(long ID) {
    	return dao.seleccionarPorID(ID);
    }
    
    public long borrar(long ID) throws Exception {
    	return dao.borrar(ID);
    }
    
    public void asociarAsignatura(long ID, long a) throws SQLException {
    	dao.AsignarAsignaturas(ID, a);
    }
    
    public void desasociarAsignatura(long ID, long a) throws SQLException {
    	dao.DesrelacionarAsignatura(ID, a);
    }
    
    public List<Asignaturas> listarAsignaturas (long cedula){
    	return dao.ListarAsignaturasByPersona(cedula);
    }
    
    public List<Persona> listarAlumnos (long a){
    	return dao.listarPersonasByAsignatura(a);
    }
    public long seleccionarCodigoCedula(long codigo, long cedula) {
    	return dao.seleccionarCodigoCedula(codigo, cedula);
    }
}
