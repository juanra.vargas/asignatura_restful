package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignaturas  implements Serializable {

	
	Long idAsignatura;
	String nombreAsignatura;
	
	List <Persona> Alumnos;
	
	public Asignaturas() {
		Alumnos = new ArrayList<Persona>();
	}
	
	public Asignaturas(Long id, String nombre) {
		this.idAsignatura = id;
		this.nombreAsignatura = nombre;
		
		Alumnos = new ArrayList<Persona>();
	}
	
	
	public Long getId() {
		return this.idAsignatura ;
	}
	
	public void setID(Long ID) {
		this.idAsignatura = ID;
	}
	
	public String getNombre() {
		return this.nombreAsignatura;
	}
	
	public void setNombre(String nombre) {
		this.nombreAsignatura = nombre;
	}
	
	public List<Persona> getAlumnos(){
		return this.Alumnos;
	}
	
	public void setAumnos(List<Persona> Alumnos) {
		this.Alumnos = Alumnos;
	}
}
